function tinhDiemTrungBinh(...danhSachDiem) {
  let sum = 0;
  danhSachDiem.forEach((diem) => (sum += diem));
  let average = sum / danhSachDiem.length;
  return average;
}

// diem Khoi lop 1
document.getElementById("btnKhoi1").onclick = () => {
  let diemToan = document.getElementById("inpToan").value.trim() * 1;
  let diemLy = document.getElementById("inpLy").value.trim() * 1;
  let diemHoa = document.getElementById("inpHoa").value.trim() * 1;

  let diemTrungBinh = tinhDiemTrungBinh(diemToan, diemLy, diemHoa);
  document.getElementById("tbKhoi1").innerText = diemTrungBinh.toFixed(2);
};
// diem Khoi lop 2
document.getElementById("btnKhoi2").onclick = () => {
  let diemVan = document.getElementById("inpVan").value.trim() * 1;
  let diemSu = document.getElementById("inpSu").value.trim() * 1;
  let diemDia = document.getElementById("inpDia").value.trim() * 1;
  let diemAnh = document.getElementById("inpEnglish").value.trim() * 1;

  let diemTrungBinh = tinhDiemTrungBinh(diemVan, diemSu, diemDia, diemAnh);
  document.getElementById("tbKhoi2").innerText = diemTrungBinh.toFixed(2);
};
