const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

//load colors to color Container
function loadColor() {
  let contentHTML = "";
  colorList.forEach((item) => {
    return (contentHTML += `<button class="color-button ${item}"></button>`);
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
}
loadColor();

// add Class Active and add class color for house
function addClassActive() {
  let idColor = document.querySelectorAll("#colorContainer button");
  let idHouse = document.getElementById("house");
  console.log("idColor: ", idColor);
  idColor[0].classList.add("active");
  for (let i = 0; i < idColor.length; i++) {
    idColor[i].addEventListener("click", () => {
      let currentActive = document.querySelector("#colorContainer .active");
      currentActive.className = currentActive.className.replace("active", "");
      idColor[i].className += " active";
      idHouse.classList = "house";
      idHouse.classList.add(`${colorList[i]}`);
    });
  }
}
addClassActive();
